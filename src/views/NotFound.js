import { Typography } from '@material-ui/core';

const Home = () => {
    return (
        <>
            <Typography variant="h4" component="h1">
                Ошибка 404. Страница не найдена 👻
            </Typography>
        </>
    );
}

export default Home;