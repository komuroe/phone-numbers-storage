import { configureStore } from '@reduxjs/toolkit';

import contactsReducer from './reducers/contactsSlice';

export default configureStore({
    reducer: {
        contacts: contactsReducer
    }
})