import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { contactAdded } from '../reducers/contactsSlice';

const AddContactForm = () => {
    const [phone, setPhone] = useState('');
    const [name, setName] = useState('');

    const dispatch = useDispatch();
    const history = useHistory();

    const onPhoneChanged = e => setPhone(e.target.value);
    const onNameChanged = e => setName(e.target.value);

    const onSaveContactClicked = () => {
        dispatch(contactAdded(name, phone));

        setPhone('');
        setName('');

        history.push(`/contacts`);
    }

    return (
        <>
            <div>
                <TextField
                    id="name"
                    label="ФИО"
                    variant="outlined"
                    value={name}
                    onChange={onNameChanged}
                />
            </div>
            <div className="top-margin">
                <TextField
                    id="phone"
                    label="Телефон"
                    variant="outlined"
                    value={phone}
                    onChange={onPhoneChanged}
                />
            </div>
            <div className="top-margin">
                <Button
                    variant="contained"
                    size="large"
                    color="primary"
                    startIcon={<SaveIcon />}
                    onClick={onSaveContactClicked}
                >
                    Сохранить
                </Button>
            </div>
        </>
    )

}

export default AddContactForm;