import { Typography } from '@material-ui/core';

const Home = () => {
    return (
        <div className="heading-wrap">
            <Typography variant="h4" component="h1">
                Добро пожаловать! 🎉
            </Typography>
        </div>
    );
}

export default Home;