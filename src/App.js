import React from 'react';
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Switch, Route, Link, BrowserRouter } from "react-router-dom";
import Paper from '@material-ui/core/Paper';

import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import HomeIcon from '@material-ui/icons/Home';
import PhoneIcon from '@material-ui/icons/Phone';

import Home from './views/Home';
import AddContactForm from './views/AddContactForm';
import ContactsCollection from './views/ContactsCollection';
import NotFound from './views/NotFound';

const allTabs = ['/', '/addContact', '/contacts'];

const  App = () => {
    return (
        <BrowserRouter>
            <div className="App">
                <Route
                    path="/"
                    render={({ location }) => (
                        <>
                            <Paper>
                                <Tabs
                                    value={location.pathname}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    centered
                                >
                                    <Tab
                                        label="Главная"
                                        value="/"
                                        component={Link}
                                        to={allTabs[0]}
                                        icon={<HomeIcon />}
                                    />
                                    <Tab
                                        label="Форма"
                                        value="/addContact"
                                        component={Link}
                                        to={allTabs[1]}
                                        icon={<PhoneIcon />}
                                    />
                                    <Tab
                                        label="Коллекция"
                                        value="/contacts"
                                        component={Link}
                                        to={allTabs[2]}
                                        icon={<FormatListBulletedIcon />}
                                    />
                                </Tabs>
                            </Paper>

                            <Paper className="tab-panel">
                                <Switch>
                                    <Route exact path={allTabs[1]} component={AddContactForm}/>} />
                                    <Route exact path={allTabs[2]} component={ContactsCollection} />
                                    <Route exact path={allTabs[0]} component={Home} />
                                    <Route component={NotFound} />
                                </Switch>
                            </Paper>
                        </>
                    )}
                />
            </div>
        </BrowserRouter>
    )
}

export default App;
