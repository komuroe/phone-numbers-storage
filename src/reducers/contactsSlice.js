import { createSlice } from '@reduxjs/toolkit';

import toCamelCase from '../utils/toCamelCase';
import toPhoneNumber from '../utils/toPhoneNumber';

const initialState = [];

const contactsSlice = createSlice({
    name: 'contacts',
    initialState,
    reducers: {
        contactAdded: {
            reducer(state, action) {
                state.push(action.payload)
            },
            prepare(name, phone) {
                return {
                    payload: {
                        name: toCamelCase(name),
                        phone: toPhoneNumber(phone)
                    }
                }
            }
        },
        contactRemoved: {
            reducer(state, action) {
                state.splice(action.payload, 1);
            }
        }
    }
})

export const { contactAdded, contactRemoved } = contactsSlice.actions;

export default contactsSlice.reducer