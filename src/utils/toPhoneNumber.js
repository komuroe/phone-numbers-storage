// Параметр numbers - строка из 10 символов
// её необходимо преобразовать в вид '7 (xxx) xxx-xxxx'
const toPhoneNumber = (numbers) => {
    let match = numbers.match(/^(\d{3})(\d{3})(\d{4})$/);

    return (match)? `7 (${match[1]}) ${match[2]}-${match[3]}` : '';
}

export default toPhoneNumber;