// Параметр user - строка с разделителями "-" или "_" следующего вида
// "имя-фамилия-отчество", "имя_Фамилия-отчество", "Имя_фамилия_Отчество"
// необходимо вернуть новую строку в формате CamelCase и пробелом как разделитель
// подробнее - https://ru.wikipedia.org/wiki/CamelCase
const toCamelCase = (user) => {
    if (!user) return '';

    const splittedWords =  user.split(/[-_]+/g);
    const capitalizedRestWords = splittedWords.slice(1).map(word => (word.charAt(0).toUpperCase() + word.slice(1)));

    return `${splittedWords[0]} ${capitalizedRestWords.join(" ")}`;
}

export default toCamelCase;