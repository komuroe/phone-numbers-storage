import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Typography } from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import { contactRemoved } from '../reducers/contactsSlice';

import ClearIcon from '@material-ui/icons/Clear';

const ContactsCollection = () => {
    const dispatch = useDispatch();

    const deleteItem = (i) => {
        dispatch(contactRemoved(i));
    };

    const contacts = useSelector(state => state.contacts);

    return (
        <>
            {contacts.length
                ? (
                   <div className="table-wrap">
                       <Paper variant="outlined">
                           <Table>
                               <TableHead>
                                   <TableRow>
                                       <TableCell>Имя</TableCell>
                                       <TableCell>Телефон</TableCell>
                                       <TableCell>Удалить</TableCell>
                                </TableRow>
                               </TableHead>
                               <TableBody>
                                {contacts.map((item, i) => {
                                    return (
                                        <TableRow key={`row-${i}`}>
                                            <TableCell>{item.name}</TableCell>
                                            <TableCell>{item.phone}</TableCell>
                                            <TableCell>
                                                <Button
                                                    onClick={deleteItem.bind(this, i)}
                                                    color='secondary'
                                                >
                                                    <ClearIcon />
                                                </Button>
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </Paper>
                   </div>)
                : (
                    <Typography variant="h5" component="h1">
                        Нет контактов для отображения.
                    </Typography>
                )
            }
        </>

    );
}

export default ContactsCollection